import React from 'react';
class NotifyChange extends React.Component {
  constructor (props) {
    super(props)
    this.state = { value: props.defaultValue }
  }

  handleChange (event) {
    let input = event.target
    let value = input.value
    // 值校验
    if (!/^1[1-9][0-9]{0,10}/.test(value)) {
      input.value = value
    } else {
      // 更新组件自身内部的状态
      this.setState({
        value: value
      })
      // 同时，也通知父组件值已经被更改
      this.props.onChange(value)
    }
  }

  render () {
    // 当点击button被点击时，将调用props传入的callParent方法
    return <input type="tel" value={this.state.value} onChange={this.handleChange.bind(this)} />
  }
}
export default NotifyChange
