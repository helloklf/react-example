import React from 'react';

class DataTransfer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    // 当点击button被点击时，将调用props传入的callParent方法
    return <button onClick={this.props.callParent}>点我</button>
  }
}

export default DataTransfer
