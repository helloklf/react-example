import React from 'react'

class LiftingStateUpOne extends React.Component {
  plus (event) {
    let value = this.props.value + 1
    this.props.onChange(value)
  }

  render () {
    return  (
      <div>
        <span>已用：{this.props.value}</span>
        <span>{this.props.currentValue}</span>
        <span className="plus" onClick={this.plus.bind(this)}>+</span>
      </div>
    )
  }
}

export default LiftingStateUpOne
