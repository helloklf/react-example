import React, { Component } from 'react';
import NameForm from './example/NameForm'
import DataTransfer from './example/DataTransfer'
import NotifyChange from './example/NotifyChange'
import LiftingStateUpOne from './example/LiftingStateUpOne'
import LiftingStateUpTwo from './example/LiftingStateUpTwo'
import './example/LiftingStateUp.css'
import './App.css';

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      a: null,
      total: 100,
      left: 1,
      right: 99
    }
  }

  onInputChange_1 (event) {
    this.setState({
      a: event.target.value
    })
    event.preventDefault();
  }

  callParent () {
    alert('这个方法传给子组件调用了！')
  }

  onInputChange (value) {
    alert('子组件已经修改了值，当前值：' + value)
  }

  onLeftChange (value) {
    this.setState((state, props) => {
      return {
        left: value,
        right: state.total - value
      }
    })
  }

  onRightChange (value) {
    this.setState((state, props) => {
      return {
        left: state.total - value,
        right: value
      }
    })
  }

  render () {
    return (
      <div>
        <input defaultValue="true"></input>
        {/*受控组件用法*/}
        <input value={this.state.a} onChange={this.onInputChange_1.bind(this)}></input>
        <br />
        <NameForm />

        <DataTransfer callParent={ this.callParent.bind(this) } />
        <NotifyChange defaultValue='15116669012' onChange={this.onInputChange.bind(this)} />

        <br />
        <h4>状态提升示例：</h4>
        <div>总共：{this.state.total}</div>
        <LiftingStateUpOne value={this.state.left} onChange={this.onLeftChange.bind(this)} />
        <LiftingStateUpTwo value={this.state.right} onChange={this.onRightChange.bind(this)} />
      </div>
    )
  }
}

export default App;
