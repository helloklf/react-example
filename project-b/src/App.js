import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  testClick (a, b) {
    console.log(a, b)
  }

  touchStart () {
    alert(1)
  }

  render() {
    function UserGreeting(props) {
      if (!props.isLoggedIn) { return null }
      return <h1>欢迎回来</h1>;
    }
    function GuestGreeting(props) {
      if (props.isLoggedIn) { return null }
      return <h1>请登录</h1>;
    }
    let list = [1, 2];
    let listItems = list.map((id) => {
      return <h2 onTouchStart={this.touchStart} key={id.toString()}>{id}</h2>
    })
    const listItems2 = [];
    for (let i=0; i<list.length; i++) {
      listItems.push(<h2>{list[i]}</h2>)
    }
    let isLoggedIn = true
    return (
      <div className="App">
        <UserGreeting isLoggedIn={isLoggedIn} />
        <GuestGreeting isLoggedIn={isLoggedIn} />
        {
          listItems2
        }
        {
          isLoggedIn && <div>已登录</div>
        }
        <div>{true}-{false}-{1}</div>
        <header className="App-header">
          <img onClick={this.testClick.bind(this)} src={logo} className="App-logo" alt="logo" />
          {listItems}
          <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
