#### 组件
- 我们希望通过React，尽量将页面元素拆分成可复用的部件
- 例如具有客制化样式的按钮或输入框

#### 函数组件
- 这是最简单的一种React组件定义方式，通过参数props读取传入的属性
```javascript
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
```

#### ES6语法声明组件
- 通过继承```React.Component```
- 例如下面的代码，和上面的函数组件效果是同样的
```javascript
class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```

#### 使用组件
- 一个最基本的组件使用例子如下：
```javascript
// 通过props读取参数
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
ReactDOM.render(<Welcome name="张飞" />, document.getElementById('root'));
```
- 前面提到，在JSX中，```const element = <div />;```这种定义是合理的
- 同样，```const element = <Welcome name="张飞" />```这样的语法也是合理的
- 完整示例如：
```javascript
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

const element = <Welcome name="张飞" />;
ReactDOM.render(element, document.getElementById('root'));
```

#### Props是只读的
- 无论是通过Function还是Class定义的组件，其props都是只读的，不要企图更改props的值
- 如果你妄想更改props，将获得```TypeError: Cannot assign to read only property 'name' of object '#<Object>'```这样的错误


#### State - 组件状态
- 添加构造函数，在构造函数里初始化```this.state```
```javascript
class Clock extends React.Component {
  constructor(props) {
    super(props); //
    this.state = {date: new Date()};
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}
```
- 进而，更改state，并触发重新渲染
```javascript
// 错误示例：不会触发重新渲染组件
this.state.date = new Date();
// 正确示例，使用setState更改state
this.setState({ date: new Date() })
```

- 可以通过通过setState单独更新某些值，例如：
```javascript
// state定义
constructor(props) {
  super(props);
  this.state = {
    posts: [],
    comments: []
  };
}

// 更新comments，而不需传入整个state的内容
this.setState({
  comments: response.comments
});
```

- setState合并值的层次是很浅的，例如下面的例子：
```javascript
// state定义
constructor(props) {
  super(props);
  this.state = {
    books: {
      a: 1,
      b: 2
    }
  };
}

// 这个操作会直接替换整个books对象，而不是只更新books.a的值
this.setState({
  books: { a: 1 }
});
```

#### setState的异步机制
- React会将多次setState调用合并成单次来提高性能
- 因此，setState可能不会立即生效
- 如果你想避免异步更新带来的问题，可将setState调用改为下面的形式，传入一个函数，而不是一个对象
```javascript
// Correct
this.setState((state, props) => return { counter: state.counter + props.increment });
```


#### 生命周期
- componentDidMount()
- componentWillUnmount() 
```javascript
class Clock extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    // 组件被渲染到DOM上时执行
  }

  componentWillUnmount () {
    // 组件被移除时执行
  }

  render() {
    return (
      <h1>Hello, world!</h1>
    );
  }
}
```
- 详细的声明周期，可参考下图：
![生命周期](./imgs/588767-20161205190022429-1074951616.jpg)